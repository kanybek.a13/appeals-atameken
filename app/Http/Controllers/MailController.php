<?php

namespace App\Http\Controllers;

use App\Mail\RegisterEmail;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function sendRegisterController($name, $email){
        $template = 'vendor.email.register';
        $data = [
            'name' => '123123',
            'login' => 'kanybek.a13',
            'password' => 'vjvgscljldqmjoqz',
            'link' => env('APP_URL') . "/ru/login",
        ];
        $from = 'kanybek.a13@gmail.com';
        $to = $email;
        $subject = __('default.notifications.register.title') . ' | ' . date('d-m-Y H:i:s');

        try {
            Mail::send($template, $data, static function ($message) use ($from, $to, $subject) {
                $message->from($from, 'no-reply');
                $message->to($to);
                $message->subject($subject);
            });
        } catch (\Swift_TransportException $e) {
            dd($e);
        } catch (ConnectException $b) {
            dd($b);
        }
    }
}
