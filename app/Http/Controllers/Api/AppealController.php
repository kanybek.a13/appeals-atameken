<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppealRequest;
use App\Models\Appeal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AppealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return response([
            'error' => false,
            'appeals' => Appeal::where('status', '>', '-1')->get(),
            'message' => 'Retrieved successfully'
        ], 200);
    }

    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appeal_id' => 'required|integer',
        ]);

        if ($validator->fails()){
            return response([
                'error' => true,
                'message' => 'appeal_id is required',
            ]);
        }

        $appeal = Appeal::find($request['appeal_id']);

        if ($appeal){
            return response([
                'error' => false,
                'message' => 'Retrieved successfully',
                'appeal' => $appeal,
            ], 200);
        }

        return response([
            'error' => true,
            'message' => 'Appeal not found',
        ], 404);
    }

    public function store(AppealRequest $request)
    {
        $data = $request->validated();

        $appeal = Appeal::create([
            'text' => $data['text'],
            'owner_id' => auth()->user()->id
        ]);

        return response([
            'error' => false,
            'message' => 'Created successfully',
            'appeal' => $appeal,
        ], 201);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'text' => 'required|max:255',
            'appeal_id'=>'required'
        ]);

        $appeal = Appeal::find($data['appeal_id']);

        if ($appeal){
            $appeal->update($data);

            return response([
                'error' => false,
                'appeal' => $appeal,
                'message' => 'Updated successfully'
            ], 200);
        }

        return response([
            'error' => true,
            'message' => 'Appeal not found'
        ], 404);

    }

    public function destroy(Request $request)
    {
        $data = $request->validate([
            'appeal_id' => 'required|integer',
        ]);

        $appeal = Appeal::find($data['appeal_id']);

        if ($appeal){
            $appeal->delete();

            return response([
                'error' => false,
                'message' => 'Deleted successfully'
            ], 200);
        }

        return response([
            'error' => true,
            'message' => 'Appeal not found'
        ], 404);
    }}
