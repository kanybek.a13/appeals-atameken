<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $validatedData = $request->validated();

        $validatedData['password'] = Hash::make($request->password);

        $api_token = Str::random(60);

        $user = User::create($validatedData);
        $user->update(['api_token'=> $api_token]);

        return response(['user'  => $user, 'api_token' => $user->api_token], 201);
    }

    public function login(LoginRequest $request)
    {
        $data = $request->validated();

        if (!auth()->attempt($data)) {
            return response([
                "message"=>'wrong password or username',
                "error" => true
            ],'401');
        }

        $api_token = Str::random(60);
        auth()->user()->update(['api_token'=> $api_token]);

        return response([
            'user' => auth()->user(),
            'api_token' => auth()->user()->api_token
        ]);
    }
}
