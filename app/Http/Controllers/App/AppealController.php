<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Appeal;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AppealController extends Controller
{
    public function index()
    {
        if (request('text')!='') {
            $appeals = Appeal::where('text', 'Like', '%' . request('text') . '%');
        }else
            $appeals = Appeal::whereNotNull('text');

        if (request('status')==='all') {
            $appeals = $appeals->where('status', '<>', -1);
        }elseif (request('status')!=''){
            $appeals = $appeals->where('status', request('status'));
        }else
            $appeals = $appeals->where('status', 1);

        if (request('dateFrom')!='') {
            $appeals = $appeals->where('created_at', '>=', request('dateFrom') );
        }

        if (request('dateTo')!='') {
            $appeals = $appeals->where('created_at', '<=', request('dateTo') );
        }

        $appeals = $appeals->latest('updated_at')
            ->paginate(6);

        return view('app.appeal.index', compact('appeals'));
    }

    public function create()
    {
        return view('app.appeal.create');
    }

    public function store(Request $request)
    {
        $attributes = $this->validateRequest();

        $appeal = auth()->user()->appeals()->create([
            'text'=>$attributes['text']
        ]);

        if( $request->hasFile('files')) {
            foreach ($request->file('files') as $file)

                $path = public_path(). '/uploads/';
                $filename = $file->GetClientOriginalName();

                $file->move($path, $filename);

                File::create([
                    'name' => $filename,
                    'path' => '/uploads/' . $filename,
                    'appeal_id' => $appeal->id
                ]);
        }

        return redirect($appeal->path());
    }

    public function show(Appeal $appeal)
    {
        return view('app.appeal.show', compact('appeal'));
    }

    protected function validateRequest(): array
    {
        return \request()->validate([
            'text' => 'required',
            'files' => 'sometimes|required:jpg,jpeg,bmp,png,doc,docx,csv,pdf',

//            'file' => 'sometimes|required:jpeg,png,jpg,gif,svg|max:2048',
//            'file' => 'sometimes|required:jpeg,jpg,png,gif' // max 10000kb
        ]);
    }
}
