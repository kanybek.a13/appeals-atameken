<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Appeal;
use App\Models\AppealRequests;
use Illuminate\Http\Request;

class AppealRequestsController extends Controller
{
    public function store(Appeal $appeal){
        $validatedData = $this->validateAppealInfo(\request());

        $appealRequest = $appeal->requests()->create([
            'text' => $validatedData['text'],
            'owner_id' => auth()->user()->id
        ]);

        $appeal->complete();

        return redirect($appeal->path());
    }

    public function delete(Appeal $appeal, AppealRequests $appealRequest){
        $appealRequest->delete();

        return redirect($appeal->path());
    }

    public function validateAppealInfo($data){
        return request()->validate([
            'text' => 'required',
        ]);
    }}
