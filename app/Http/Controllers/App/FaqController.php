<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::all();

        return view('app.faq', compact('faqs'));
    }
}
