<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\User;
//use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $data = $this->validateRequest();

        $registeredUser = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'patronymic' => $data['patronymic'],
            'iin' => $data['iin'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return view('app.index');
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update( Request $request, User $user)
    {
        $data = $this->validateRequest($request, $user);
//          $data = Validator::make( $request->all(), $user->rules())->validated();

        $updatedUser = $user->update([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'patronymic' => $data['patronymic'],
            'iin' => $data['iin'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->back()->with('success', 'Updated successfully');
    }

    /**
     * @param $request
     * @param $user
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateRequest($request, $user)
    {
        return $validator = Validator::make( $request->all(), $user->rules())->validate();

//        return \request()->validate([
//            'name' => ['required', 'string', 'max:255'],
//            'surname' => ['required', 'string', 'max:255'],
//            'patronymic' => ['required', 'string', 'max:255'],
//            'iin' => 'required', 'string', 'max:12', 'min:12', Rule::unique('users', 'iin')->ignore($user->id),
////            'iin' => ['required', 'string', 'max:12', 'min:12', 'unique:users'],
//            'phone' => ['required', 'string', 'max:16'],
//            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore($user->id)],
////            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'confirmed'],
//        ]);
    }
}
