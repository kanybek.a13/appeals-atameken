<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Appeal;
use Illuminate\Http\Request;

class CabinetController extends Controller
{
    public function edit()
    {
        return view('app.cabinet.edit', [
            'user' => auth()->user()
        ]);
    }

    public function appeals()
    {
        $appeals = Appeal::where('owner_id', auth()->user()->id)
            ->latest('updated_at')
            ->paginate(5);

        return view('app.cabinet.appeals', compact('appeals'));
    }
}
