<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index(){
        if (request('text')) {
            $faqs = Faq::where('question', 'Like', '%' . request('text') . '%')
                ->orWhere('answer', 'Like', '%' . request('text') . '%');
        }else
            $faqs = Faq::whereNotNull('id');

        $faqs = $faqs->latest('updated_at')
            ->paginate(5);

        return view('admin.faq.index', compact('faqs'));
    }

    public function show(Faq $faq){
        return view('admin.faq.show', [
            'faq'=> $faq
        ]);
    }

    public function edit(Faq $faq){
        return view('admin.faq.edit',[
            'faq' => $faq
        ]);
    }

    public function new(){
        return view('admin.faq.new');
    }

    public function update(Faq $faq){
        $validatedData = $this->validateFaqInfo(\request());

        $faq->update($validatedData);

        return redirect('/admin/faqs');
    }

    public function store(){
        $validatedData = $this->validateFaqInfo(\request());

        $faq = new Faq;

        $faq->question = $validatedData['question'];
        $faq->answer = $validatedData['answer'];

        $faq->save();

        return redirect('/admin/faqs');
    }

    public function delete(Faq $faq){
        $faq->delete();

        return redirect('/admin/faqs');
    }

    public function validateFaqInfo($data){
        return request()->validate([
            'question' => 'required',
            'answer' => 'required',
        ]);
    }
}

