<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Appeal;
use App\Models\AppealRequests;
use App\Models\File;
use Illuminate\Http\Request;

class AppealController extends Controller
{
    public function index(){
        if (request('text')!='') {
            $appeals = Appeal::where('text', 'Like', '%' . request('text') . '%');
        }else
            $appeals = Appeal::whereNotNull('text');

        if (request('status')==='all') {
            $appeals = $appeals->where('status', '>=', '-1');
        }elseif (request('status')!=''){
            $appeals = $appeals->where('status', request('status'));
        }

        if (request('dateFrom')!='') {
            $appeals = $appeals->where('created_at', '>=', request('dateFrom') );
        }

        if (request('dateTo')!='') {
            $appeals = $appeals->where('created_at', '<=', request('dateTo') );
        }

        $appeals = $appeals->latest('updated_at')
            ->paginate(5)
            ->appends(request()->query());;

        return view('admin.appeal.index',
            compact('appeals'));
    }

    public function show(Appeal $appeal){
        return view('admin.appeal.show', [
            'appeal'=> $appeal
        ]);
    }

    public function edit(Appeal $appeal){
        return view('admin.appeal.edit',[
            'appeal' => $appeal
        ]);
    }

    public function new(){
        return view('admin.appeal.new');
    }

    public function update(Appeal $appeal){
        $attributes = $this->validateAppealInfo(\request());

        $appeal->update([
            'text' => $attributes['text'],
            'status' => \request('status')
        ]);

        return redirect('/admin/appeals');
    }

    public function store(Request $request)
    {
        $attributes = $this->validateRequest();

        $appeal = auth()->user()->appeals()->create(['text'=>$attributes['text']]);

        if( $request->hasFile('files')) {
            foreach ($request->file('files') as $file)

                $path = public_path(). '/uploads/';
            $filename = $file->GetClientOriginalName();

            $file->move($path, $filename);

            $uploadedFile = File::create([
                'name' => $filename,
                'path' => $path,
                'appeal_id' => $appeal->id
            ]);
        }

        return redirect($appeal->path());
    }

    public function delete(Appeal $appeal){
        $appeal->delete();

        return redirect('/admin/appeals');
    }

    public function deleteFile(Appeal $appeal, File $file){
        $file->delete();

        return redirect('/admin' .  $appeal->path());
    }

    public function validateAppealInfo($data){
        return request()->validate([
            'text' => 'required',
        ]);
    }
}
