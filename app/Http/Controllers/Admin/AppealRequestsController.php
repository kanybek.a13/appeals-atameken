<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Appeal;
use App\Models\AppealRequests;
use Illuminate\Http\Request;

class AppealRequestsController extends Controller
{
    public function store(Appeal $appeal){
        $validatedData = $this->validateAppealInfo(\request());

        $appealRequest = AppealRequests::create([
            'text' => $validatedData['text'],
            'appeal_id' => $appeal->id,
            'owner_id' => auth()->user()->id
        ]);

        $appeal->changeStatus($validatedData['status']);

        return redirect('/admin' . $appeal->path());
    }

    public function delete(Appeal $appeal, AppealRequests $appealRequest){
        $appealRequest->delete();

        return redirect('/admin' .  $appeal->path());
    }

    public function validateAppealInfo($data){
        return request()->validate([
            'text' => 'required',
            'status' => 'required',
        ]);
    }
}

