<?php

namespace App\Http\Controllers;

use App\Models\Appeal;
use App\Models\Log;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function adminIndex()
    {
        return view('admin.index');
    }

    public function appIndex()
    {
//        $template = 'vendor.email.register';
//        $data = [
//            'name' => '123123',
//            'login' => 'kanybek.a13',
//            'password' => 'qwertyuiop132698',
//            'link' => env('APP_URL') . "/ru/login",
//        ];
//        $from = 'kanybek.a13@gmail.com';
//        $to = 'kanybek.a13@mail.ru';
//        $subject = __('default.notifications.register.title') . ' | ' . date('d-m-Y H:i:s');
//
//        try {
//            Mail::send($template, $data, static function ($message) use ($from, $to, $subject) {
//                $message->from($from, 'no-reply');
//                $message->to($to);
//                $message->subject($subject);
//            });
//        } catch (\Swift_TransportException $e) {
//            dd($e);
//        } catch (ConnectException $b) {
//            dd($b);
//        }

//        dd(phpinfo());

        $appeals = Appeal::where('status', '>=' , 1)
            ->latest('updated_at')
            ->take(9)
            ->get();

        $all_appeals = Appeal::where('status', '<>', -1)->count();
        $completed_appeals = Appeal::where('status', 1)->count();
        $incomplete_appeals = $all_appeals-$completed_appeals;

        return view('app.index', [
            'appeals' => $appeals,
            'all_appeals' => $all_appeals,
            'incomplete_appeals' => $incomplete_appeals,
            'completed_appeals' => $completed_appeals
        ]);
    }

    public function setLocale($locale){
        App::setLocale($locale);
        session(['locale'=>$locale]);

        return redirect()->back();
    }

    public function home()
    {
        return view('home');
    }
}
