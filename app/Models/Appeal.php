<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appeal extends Model
{
    use HasFactory;

//    protected $guarded = ['status'];

    protected $fillable = [
        'text', 'status', 'owner_id'
    ];

    public function path()
    {
        return "/appeal/{$this->id}";
    }

    public function complete()
    {
        $this->update(['status' => 1]);
    }

    public function changeStatus($status)
    {
        $this->update(['status' => $status]);
    }

    public function completed()
    {
        return Appeal::where('status', -1)
            ->latest('updated_at')
            ->take(9)
            ->get();
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function requests()
    {
        return $this->hasMany(AppealRequests::class, 'appeal_id')->latest('created_at');
    }
}
