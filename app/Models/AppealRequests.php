<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppealRequests extends Model
{
    use HasFactory;

    protected $fillable = [
        'text', 'appeal_id' , 'owner_id'
    ];
}
