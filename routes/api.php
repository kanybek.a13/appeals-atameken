<?php

//use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Api\AppealController;
use App\Http\Controllers\Api\AuthController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group( ['middleware' => 'auth:api', 'api_token'], function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::prefix('appeals')->group(function () {
        Route::get('/', [AppealController::class, 'index']);
        Route::get('/get', [AppealController::class, 'get']);
        Route::post('/store', [AppealController::class, 'store']);
        Route::put('/update', [AppealController::class, 'update']);
        Route::delete('/delete', [AppealController::class, 'destroy']);
    });
});
