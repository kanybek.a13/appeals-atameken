<?php

use App\Http\Controllers\App\AppealController;
use App\Http\Controllers\App\AppealRequestsController;
use App\Http\Controllers\App\CabinetController;
use App\Http\Controllers\App\FaqController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;

Route::post('/user/register', [\App\Http\Controllers\App\UserController::class, 'create']);

Route::get('/', [HomeController::class, 'appIndex']);

Route::get('/appeals', [AppealController::class, 'index']);
Route::get('/appeal/{appeal}', [AppealController::class, 'show']);
Route::get('/create-appeal', [AppealController::class, 'create'])->middleware(['auth', 'verified']);

Route::get('/faq', [FaqController::class, 'index']);

Route::group( ['middleware' => 'auth', 'verified'], function() {
    Route::put('/user/{user}/update', [\App\Http\Controllers\App\UserController::class, 'update']);

    Route::post('/appeals', [AppealController::class, 'store']);
    Route::post('/appeal/{appeal}/request',  [AppealRequestsController::class, 'store']);

    Route::get('/cabinet-settings', [CabinetController::class, 'edit']);
    Route::get('/cabinet-appeals', [CabinetController::class, 'appeals']);
});

//-----------------------------------------------------------------------------------

Route::get('/admin/login',  function (){
    return view('admin.login');
});

Route::group(['middleware' => ['auth', 'verified']], function (){

    Route::get('/admin/index',  [HomeController::class, 'adminIndex']);

    Route::get('/admin/faqs',  [\App\Http\Controllers\Admin\FaqController::class, 'index']);
    Route::get('/admin/faq/new',  [\App\Http\Controllers\Admin\FaqController::class, 'new']);
    Route::get('/admin/faq/{faq}',  [\App\Http\Controllers\Admin\FaqController::class, 'show']);
    Route::get('/admin/faq/{faq}/edit',  [\App\Http\Controllers\Admin\FaqController::class, 'edit']);

    Route::put('/admin/faq/{faq}',  [\App\Http\Controllers\Admin\FaqController::class, 'update']);
    Route::post('/admin/faqs',  [\App\Http\Controllers\Admin\FaqController::class, 'store']);
    Route::post('/admin/faq/{faq}/delete',  [\App\Http\Controllers\Admin\FaqController::class, 'delete']);

    Route::get('/admin/appeals',  [\App\Http\Controllers\Admin\AppealController::class, 'index'])->name('appeals');
    Route::get('/admin/appeal/{appeal}',  [\App\Http\Controllers\Admin\AppealController::class, 'show']);
    Route::get('/admin/appeal/{appeal}/edit',  [\App\Http\Controllers\Admin\AppealController::class, 'edit']);

    Route::put('/admin/appeal/{appeal}',  [\App\Http\Controllers\Admin\AppealController::class, 'update']);
    Route::post('/admin/appeals',  [\App\Http\Controllers\Admin\AppealController::class, 'store']);
    Route::post('/admin/appeal/{appeal}/delete',  [\App\Http\Controllers\Admin\AppealController::class, 'delete']);

    Route::post('/admin/appeal/{appeal}/request',  [\App\Http\Controllers\Admin\AppealRequestsController::class, 'store']);

    Route::post('/admin/appeal/{appeal}/file/{file}/delete',  [\App\Http\Controllers\Admin\AppealController::class, 'deleteFile']);
    Route::post('/admin/appeal/{appeal}/request/{request}/delete',  [\App\Http\Controllers\Admin\AppealRequestsController::class, 'delete']);

    Route::get('/admin/users', [UserController::class, 'index']);
    Route::get('/admin/new/user', [UserController::class, 'new']);
    Route::get('/admin/user/{user}', [UserController::class, 'show']);
    Route::get('/admin/user/edit/{user}', [UserController::class, 'edit']);
    Route::delete('/admin/user/delete/{user}', [UserController::class, 'delete'])->name('admin.user.delete');
    Route::put('/admin/user/{user}', [UserController::class, 'update']);
    Route::post('/admin/user', [UserController::class, 'store']);
});

//---------------------------------------------------------------------------------------

Route::get('/locale/{locale}', [HomeController::class, 'setLocale'])->name('locale');

Route::get('/home', [HomeController::class, 'home'])->name('home')->middleware('verified');
Route::get('/welcome', function (){
    return view('welcome');
});

Auth::routes(['verify'=>true]);

Auth::routes();
