</main>

<footer class="footer">
    <div class="container">
        <div class="copyright">© 2013 - 2019 — Национальная палата предпринимателей РК «Атамекен». Все права защищены.</div>
    </div>
</footer>
</div>
<script src="/app/libs/jquery/dist/jquery.js"></script>
<script src="/app/js/scripts.js"></script>
<script src="/app/js/validate.js"></script>
<script src="/app/libs/maskedinput/maskedinput.js"></script>
<script src="/app/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/app/libs/chosen/chosen.jquery.js"></script>
<script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>


<!--Only this page's scripts-->
    @yield('content')
<!---->


<div id="registration" style="display:none;" class="modal">
    <h3 class="title-primary modal__title text-center">Регистрация</h3>
{{--    <form action="/user/register" method="POST">--}}
        <form method="POST" action="{{ route('register') }}">

        @csrf

        <div class="input-group">
            <input type="text" name="surname" class="input-regular" placeholder="Фамилия" value="{{old('surname')}}" required>
            <div class="alert alert-danger">
                Это поле обязательно для заполнения
            </div>
        </div>

        <div class="input-group">
            <input type="text" name="name" class="input-regular" placeholder="Имя" value="{{old('name')}}" required>
            <div class="alert alert-danger">
                Это поле обязательно для заполнения
            </div>
        </div>

        <div class="input-group">
            <input type="text" name="patronymic" class="input-regular" placeholder="Отчество" value="{{old('patronymic')}}" required>
            <div class="alert alert-danger">
                Это поле обязательно для заполнения
            </div>
        </div>

        <div class="input-group">
            <input type="text" name="iin" class="input-regular" value="{{old('iin')}}" placeholder="ИИН" maxlength="12" minlength="12" data-validate="iin" required>
            <div class="alert alert-danger">
                <ul>
                    <li class="required">Это поле обязательно для заполнения</li>
                    <li class="length">Поле ИИН должно состоять из 12 символов</li>
                </ul>
            </div>
        </div>

        <div class="input-group">
            <input type="text" name="phone" class="input-regular" value="{{old('phone')}}" placeholder="Номер телефона" data-validate="phone" onfocus="$(this).inputmask('+7 999 999 99 99')" required>
            <div class="alert alert-danger">
                <ul>
                    <li class="required">Это поле обязательно для заполнения</li>
                    <li class="correct">Введите корректный номер телефона</li>
                </ul>
            </div>
        </div>

        <div class="input-group">
            <input type="text" name="email" class="input-regular" value="{{old('email')}}" data-validate="email" placeholder="E-mail" required>
            <div class="alert alert-danger">
                <ul>
                    <li class="required">Это поле обязательно для заполнения</li>
                    <li class="correct">Введите корректный E-mail</li>
                </ul>
            </div>
        </div>

        <div class="input-group">
            <input type="password" name="password" class="input-regular" placeholder="Введите пароль" data-validate="password" required>
            <div class="alert alert-danger">
                Это поле обязательно для заполнения
            </div>
        </div>

        <div class="input-group">
            <input type="password" name="password_confirmation" class="input-regular" placeholder="Подтвердите пароль" data-validate="password_confirmation" required>
            <div class="alert alert-danger">
                Пароли не совпадают
            </div>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                {{ __('Зарегистрироваться') }}
            </button>
            <br>
            <div class="hint-text">
                <div>Уже зарегистрированы?</div>
                <a href="#authorization" data-fancybox title="Войти">Войти</a>
            </div>
        </div>
    </form>

    @if($errors->userRegistration->any())
        <script type="text/javascript">document.getElementById('regisBtn').click()</script>
        @foreach($errors->userRegistration->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
</div>

<div id="authorization" style="display:none;" class="modal">
    <h3 class="title-primary modal__title text-center">Вход</h3>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="input-group">
            <input type="text" name="email" class="input-regular" data-validate="email" placeholder="E-mail" value="{{ old('email') }}" required autocomplete="email" autofocus>

            <div class="alert alert-danger">
                <ul>
                    <li class="required">Это поле обязательно для заполнения</li>
                    <li class="correct">Введите корректный E-mail</li>
                </ul>
            </div>
        </div>

        <div class="input-group">
            <input type="password" name="password" class="input-regular @error('password') is-invalid @enderror" placeholder="Password" required autocomplete="current-password">

            <div class="alert alert-danger">
                Это поле обязательно для заполнения
            </div>

            <div class="text-right">
                <a href="#forgot" data-fancybox="" title="Забыли пароль?" class="grey-link">Забыли пароль?</a>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn">Войти</button>
            <div class="hint-text">
                <a href="#registration" data-fancybox title="Зарегистрироваться">Зарегистрироваться</a>
            </div>
        </div>
    </form>

    @if($errors->userLogin->any())
        <script type="text/javascript">document.getElementById('loginBtn').click()</script>
        @foreach($errors->userLogin->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
</div>

<div id="forgot" style="display:none;" class="modal">
    <h3 class="title-primary modal__title text-center">Восстановление пароля</h3>
    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="input-group">
            <input id="email" type="email" name="email" class="input-regular @error('email') is-invalid @enderror" placeholder="E-mail" value="{{ old('email') }}" required autocomplete="email" autofocus>
{{--            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="text-center">
            <button type="submit" class="btn">Восстановить пароль</button>
            <div class="hint-text">
                <a href="#authorization" data-fancybox title="Войти">Войти</a>
            </div>
        </div>
    </form>
</div>

<div id="code" style="display:none;" class="modal">
    <div class="input-group__title text-center">
        На указанный вами номер телефона был выслан код подтверждения. Введите его ниже.
    </div>
    <form action="">
        <div class="input-group">
            <input type="text" name="code" class="input-regular text-center" placeholder="Введите код" required>
        </div>
        <div class="text-center">
            <button type="submit" class="btn">Подтвердить</button>
        </div>
    </form>
</div>

<div id="message" style="display:none;" class="modal">
    <div class="blue-bold text-center">
        <div>Спасибо за обращение!</div>
        <div>Номер вашего обращения 9379992</div>
    </div>
</div>

</body>
</html>
