<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>{{__('index.title')}}</title>

    <link rel="stylesheet" href="/app/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/app/libs/chosen/chosen.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/app/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/app/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/app/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/app/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/app/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/app/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/app/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/app/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/app/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/app/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/app/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/app/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/app/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/app/favicon/favicon-16x16.png">
    <link rel="manifest" href="/app/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/app/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/app/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/app/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/app/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    <header class="header compensate-for-scrollbar">
        <div class="container">
            <a href="/" title="Главная" class="logo"><img src="/app/img/logo.png" alt=""></a>
            <div class="mobile-menu">
                <nav class="menu">
                    <a href="/" title="Главная" class="active">{{__('index.main')}}</a>
                    <a href="/appeals" title="Обращения">{{__('index.appeal')}}</a>
                    <a href="/faq" title="FAQ">{{__('index.faq')}}</a>
                </nav>
                <a href="/create-appeal" title="Подать обращение" class="ghost-btn">{{__('index.submit_appeal')}}</a>
                <div class="languages">
                    <a href="{{route('locale', 'kk')}}" title="Қаз" class="{{session('locale') === 'kk' ? 'active' : ''}}">Қаз</a><span>|</span>
                    <a href="{{route('locale', 'ru')}}" title="Рус" class="{{session('locale') === 'ru' ? 'active' : ''}}">Рус</a><span>|</span>
                    <a href="{{route('locale', 'en')}}" title="Eng" class="{{session('locale') === 'en' ? 'active' : ''}}">Eng</a>
                </div>
            </div>

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>

            @if(!Auth::guest())
                <span>{{ auth()->user()->name}}!</span>
            @endif

            <div class="header-dropdown">
                <a href="javascript:;" title="" class="header-dropdown__title header-dropdown__title--icon">
                    <img src="/admin/img/user.svg" alt="">
                </a>

                <div class="header-dropdown__desc">
                    <ul>
                        <li><a href="/admin/index" title="Admin">Admin Panel</a></li>
                        @if(Auth::guest())
                            <li><a href="#registration" id="regisBtn" data-fancybox="" title="Зарегистрироваться">{{__('index.register')}}</a></li>
                            <li><a href="#authorization" id="loginBtn" data-fancybox="" title="Войти">{{__('index.login')}}</a></li>
                        @else
                            <li><a href="/cabinet-settings" title="Личные данные">{{__('index.my_personal_data')}}</a></li>
                            <li><a href="/cabinet-appeals" title="Мои обращения">{{__('index.my_appeals')}}</a></li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{__('index.logout')}}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @endif

                    </ul>
                </div>
            </div>
            <div class="menu-btn">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>

    <main class="main">


{{--        @if ($errors->any())--}}
{{--            <ul class="field mt-6 list-reset">--}}
{{--                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)--}}
{{--                    <li class="sm:text-xs text-red">{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        @endif--}}
