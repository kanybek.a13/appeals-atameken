<div class="card">
    <form action="{{ $appeal->path() . '/request'}}" method="post">
        @csrf
        @method('post')
        <div class="input-group__title">Написать коментарий</div>
        <br>
        <input type="text" name="text" class="input-regular" data-validate="email" placeholder="Техт коментарий...">
    </form>
</div>
