@include('app.layouts.header');


<section>
    <div class="container">
        <h1 class="title-primary">Часто задаваемые вопросы</h1>
        <div class="spoilers">
            @forelse($faqs as $faq)
                <div class="spoiler">
                    <div class="spoiler__title"><span>{{ $faq->question }}</span></div>
                    <div class="spoiler__desc">
                        <div class="plain-text">
                            <p>{{ $faq->answer }}</p>
                        </div>
                    </div>
                </div>
            @empty
                <div class="text-align:center">No faqs yet.</div>
            @endforelse
        </div>
    </div>
</section>

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->

    <!---->
@endsection
