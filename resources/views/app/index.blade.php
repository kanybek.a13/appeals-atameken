@include('app.layouts.header')

<section class="opening">
    <div class="container">
        <h1 class="opening__title">Виртуальная приемная Национальной палаты предпринимателей Республики Казахстан «Атамекен»</h1>
    </div>
</section>

<section>
    <div class="container">
        <div class="statistics">
            <div class="row row--multiline">
                <div class="col-xs-4">
                    <div class="stat">
                        <h4 class="stat__title">Всего обращений</h4>
                        <div class="stat__number">{{$all_appeals}}</div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="stat">
                        <h4 class="stat__title">Исполнено</h4>
                        <div class="stat__number">{{ $completed_appeals }}</div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="stat">
                        <h4 class="stat__title">На исполнении</h4>
                        <div class="stat__number">{{ $incomplete_appeals }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <h2 class="title-primary">Обращения</h2>
        <div class="row row--multiline">

            @forelse($appeals as $appeal)
                <div class="col-md-4 col-sm-6">
                    <a href="{{ $appeal->path() }}" title="" class="card">
                        <h3 class="card__title">{{ $appeal->text }}</h3>
                        <div class="card__info">
                            <div class="card__date">{{ $appeal->updated_at }}</div>
                            <div class="card__status green">{{ $appeal->status == '1' ? 'Исполнено' : 'В исполнении' }}</div>
                        </div>
                    </a>
                </div>
            @empty
                <div class="text-align:center">No appeals yet.</div>
            @endforelse

        </div>
        <br>
        <div class="text-center">
            <a href="/appeals" title="Посмотреть все обращения" class="ghost-btn">Посмотреть все обращения</a>
        </div>
    </div>
</section>

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
