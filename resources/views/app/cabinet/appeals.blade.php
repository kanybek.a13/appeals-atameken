@include('app.layouts.header');

@if(Auth::guest())
    <h2>Войдите чтобы отправить обращение</h2>
@else

<section>
    <div class="container">
        <h2 class="title-primary">Мои обращения</h2>

        <div class="row row--multiline">
            @forelse($appeals as $appeal)
                <div class="col-xs-12">
                    <a href="{{ $appeal->path() }}" title="" class="card">
                        <h3 class="card__title">{{ $appeal->text }}</h3>
                        <div class="card__info">
                            <div class="card__date">{{ $appeal->updated_at }}</div>
                            <div class="card__status green">{{ $appeal->status == '1'? 'Исполнено' : 'В исполнении' }}</div>
                        </div>
                    </a>
                </div>
            @empty
                <div class="text-align:center">No appeals yet.</div>
            @endforelse

        </div>
        <br>

        <ul class="pagination">
            <li class="previous_page"><a {{$appeals->currentPage() === 1 ? 'disabled' : '' }} href="/cabinet-appeals?page={{ $appeals->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

            @for($page = 1; $page <= $appeals->lastPage(); $page++)
                <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="/cabinet-appeals?page={{$page}}">{{ $page }}</a></li>
            @endfor

            <li class="next_page"><a {{$appeals->currentPage() === $appeals->lastPage() ? 'disabled' : '' }} href="/cabinet-appeals?page={{ $appeals->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
        </ul>
    </div>
</section>
@endif

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
