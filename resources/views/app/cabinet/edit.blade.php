@include('app.layouts.header');

<section>
    <div class="container container--sm">
        <h1 class="title-primary text-center">Личные данные</h1>
        <form class="card" action="/user/{{$user->id}}/update" method="post">
            @csrf
            @method('put')

            @if (\Session::has('success'))
                <div class="alert alert-success">
                    Данные успешно сохранены
                </div>
            @endif

            <div class="input-group">
                <label class="input-group__title">Фамилия</label>
                <input type="text" name="surname" class="input-regular" value="{{$user->surname}}" required>
                <div class="alert alert-danger">
                    Это поле обязательно для заполнения
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Имя</label>
                <input type="text" name="name" class="input-regular" value="{{$user->name}}" required>
                <div class="alert alert-danger">
                    Это поле обязательно для заполнения
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Отчество</label>
                <input type="text" name="patronymic" class="input-regular" value="{{$user->patronymic}}" required>
                <div class="alert alert-danger">
                    Это поле обязательно для заполнения
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">ИИН</label>
                <input type="text" name="iin" class="input-regular" maxlength="12" minlength="12" data-validate="iin" value="{{$user->iin}}" required>
                <div class="alert alert-danger">
                    <ul>
                        <li class="required">Это поле обязательно для заполнения</li>
                        <li class="length">Поле ИИН должно состоять из 12 символов</li>
                    </ul>
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Номер телефона</label>
                <input type="text" name="phone" class="input-regular" value="{{$user->phone}}" data-validate="phone" onfocus="$(this).inputmask('+7 999 999 99 99')" required>
                <div class="alert alert-danger">
                    <ul>
                        <li class="required">Это поле обязательно для заполнения</li>
                        <li class="correct">Введите корректный номер телефона</li>
                    </ul>
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">E-mail</label>
                <input type="text" name="email" class="input-regular" data-validate="email" value="{{$user->email}}" required>
                <div class="alert alert-danger">
                    <ul>
                        <li class="required">Это поле обязательно для заполнения</li>
                        <li class="correct">Введите корректный E-mail</li>
                    </ul>
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Поменять пароль</label>
                <input type="password" name="password" data-validate="password" class="input-regular" required>
                <div class="alert alert-danger">
                    Это поле обязательно для заполнения
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Подтвердите пароль</label>
                <input type="password" name="password_confirmation" data-validate="passwordConfirm" class="input-regular" required>
                <div class="alert alert-danger">
                    Пароли не совпадают
                </div>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <div class="text-center">
                <button type="submit" class="btn">Сохранить</button>
            </div>
        </form>
    </div>
</section>

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
