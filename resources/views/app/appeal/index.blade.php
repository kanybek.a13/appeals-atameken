@include('app.layouts.header');

<section>
    <div class="container">
        <h2 class="title-primary">Обращении</h2>

        <form action="/appeals" method="GET">
            <div class="row row--multiline">
                <div class="col-md-3 col-sm-6">
                    <div class="input-group">
                        <label class="input-group__title">Поиск по ключевому слову или фразе</label>
                        <input type="text" name="text" class="input-regular" placeholder="Введите текст"  value="{{request('text')}}">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="input-group">
                        <label class="input-group__title">Статус</label>
                        <select name="status" class="chosen no-search input-regular" data-placeholder="Все" required>
                            <option class="hidden" value="1" hidden>Исполнен</option>

                            <option value="all" {{request('status') === 'all' ? 'selected' :''}} >Все</option>
                            <option value="1" {{request('status') == '1' ? 'selected' :''}} >Исполнен</option>
                            <option value="0" {{request('status') == '0' ? 'selected' :''}} >На исполнении</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-group">
                        <label class="input-group__title">Поиск  по дате (от)</label>
                        <input type="date" name="dateFrom" class="input-regular" value="{{request('dateFrom')}}">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-group">
                        <label class="input-group__title">Поиск  по дате (до)</label>
                        <input type="date" name="dateTo" class="input-regular" value="{{request('dateTo')}}">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <label class="input-group__title hidden-xs">&nbsp;</label>
                    <button class="btn full-width" >Применить</button>
                </div>
            </div>
        </form>
        <br>

        <div class="row row--multiline">
            @forelse($appeals as $appeal)
                <div class="col-md-4 col-sm-6">
                    <a href="{{ $appeal->path() }}" title="" class="card">
                        <h3 class="card__title">{{ $appeal->text }}</h3>
                        <div class="card__info">
                            <div class="card__date">{{ $appeal->updated_at }}</div>
                            <div class="card__status green">
                                {{ $appeal->status === 1 ? 'Исполнено' : 'В исполнении' }}
                            </div>
                        </div>
                    </a>
                </div>
            @empty
                <div class="text-align:center">No appeals yet.</div>
            @endforelse
        </div>
        <br>

{{--        {{ dump(preg_replace('/page=([0-9]+)/', '',request()->getRequestUri())) }}--}}
{{--        {{dd(request()->getRequestUri())}}--}}
{{--        <ul class="pagination">--}}
{{--            <li class="previous_page"><a {{$appeals->currentPage() === 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $appeals->currentPage() - 1 }}"><i class="icon-left"></i></a></li>--}}

{{--            @for($page = 1; $page <= $appeals->lastPage(); $page++)--}}
{{--                <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="/appeals?page={{$page}}">{{ $page }}</a></li>--}}
{{--                <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="{{ preg_replace('/page=([0-9]+)/', '',request()->getRequestUri()) }}?page={{$page}}">{{ $page }}</a></li>--}}
{{--                <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="{{request()->getRequestUri()}}?page={{$page}}">{{ $page }}</a></li>--}}
{{--            @endfor--}}

{{--            <li class="next_page"><a {{$appeals->currentPage() === $appeals->lastPage() ? 'disabled' : '' }} href="/appeals?page={{ $appeals->currentPage() + 1 }}"><i class="icon-right"></i></a></li>--}}
{{--        </ul>--}}

        <ul class="pagination">
            <li class="next_page"><a href="/appeals?page={{ $appeals->currentPage() > 1 ? $appeals->currentPage() - 1 : 1 }}{{'&key='.(request()->key ?? '').'&status='.(request()->status ?? '').'&dateFrom='.(request()->dateFrom ?? '').'&dateTo='.(request()->dateTo ?? '') }}"><i class="icon-left"></i></a></li>
                @for($k = $appeals->currentPage() - 2, $i = $k <= 0 ? 1 : $k; $i <= $appeals->currentPage() + ($k <= 0 ? 4 : 2); $i++)
                    @if($i <= $appeals->lastPage())
                        <li><a {{ $i === $appeals->currentPage() ? 'class="active"' : '' }}href="/appeals?page={{$i}}&key={{ request()->key ?? '' }}">{{ $i }}</a></li>
                    @endif
                @endfor
            <li class="next_page"><a href="/appeals?page={{ $appeals->currentPage() < $appeals->lastPage() ? $appeals->currentPage() + 1 : $appeals->lastPage() }}{{'&key='.(request()->key ?? '').'&status='.(request()->status ?? '').'&dateFrom='.(request()->dateFrom ?? '').'&dateTo='.(request()->dateTo ?? '') }}"><i class="icon-right"></i></a></li>
        </ul>
    </div>
</section>


@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
