@include('app.layouts.header')

@if(Auth::guest())
    <h2>Войдите чтобы отправить обращение</h2>
@else
    <section>
        <div class="container">
            <h1 class="title-primary">Подача обращения</h1>
            <form class="card" method="post" action="/appeals" enctype="multipart/form-data">
                @csrf
                @method('post')

                <div class="input-group">
                    <textarea name="text" placeholder="Текст обращения" class="input-regular" required></textarea>
                </div>

                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить документы</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="files[]" multiple >
                    </label>
                </div>

                <div class="input-group">
                    <label class="checkbox">
                        <input type="checkbox" name="agree" id="agree">
                        <span>Я согласен на обработку предоставленной мной информации</span>
                    </label>
                </div>

                <button class="btn" disabled>Отправить</button>
            </form>
        </div>
    </section>
@endif

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->

    <!---->
@endsection
