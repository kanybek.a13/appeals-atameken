@include('app.layouts.header');

<section>
    <div class="container">
        <h1 class="title-primary">Обращение</h1>
        <div class="card">
            <div class="card__info">
                <div class="card__date">{{$appeal->created_at}}</div>
                <div class="card__status green">{{$appeal->status == '1' ? "Исполнено" : "В исполнении"}}</div>
            </div>
            <div class="input-group__title">Текст обращения</div>
            <div class="plain-text">
                <p><strong>{{$appeal->text}}</strong></p>
            </div>
            <div class="input-group__title">Прикрепленные файлы</div>
            <div class="documents">
                @forelse($appeal->files as $file)
                    <a href="{{$file->path}}" title=""><i class="icon-file"></i> <span>{{$file->name}}</span></a>
                @empty
                    <p>No files</p>
                @endforelse
            </div>
        </div>
        <div class="card">
            <div class="input-group__title">Ответы на обращение</div>
            @forelse($appeal->requests as $appeal_request)
                <hr>
                <div class="plain-text">
                    <p>{{$appeal_request->created_at}}</p>
                    <p>{{$appeal_request->text}}</p>
                </div>
            @empty
                <div class="plain-text">
                    <p>No comments yet!</p>
                </div>
            @endforelse
        </div>

        @if (auth()->user() and (Auth::user()->can('edit_content') or auth()->user()->id===$appeal->owner_id))
            @include('app.layouts.comment')
        @endif

{{--        @if (Auth::user()->can('edit_content'))--}}
{{--            @include('app.layouts.comment')--}}
{{--        @elseif(auth()->user()->id===$appeal->owner_id)--}}
{{--            @include('app.layouts.comment')--}}
{{--        @endif--}}
    </div>
</section>

@extends('app.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
