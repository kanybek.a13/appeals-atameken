@include('admin.layouts.header')
@can('admin')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Пользователи</h2>

            <form action="/admin/users" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">ФИО</label>
                            <input type="text" name="name" class="input-regular" placeholder="Введите ФИО"  value="{{request('name')}}">
                        </div>
                    </div>
{{--                    <div class="col-md-3 col-sm-6">--}}
{{--                        <div class="input-group">--}}
{{--                            <label class="input-group__title">Роль</label>--}}
{{--                            <select name="role" class="chosen no-search input-regular" data-placeholder="Все" required>--}}
{{--                                <option value="all" {{request('role') === 'all' ? 'selected' :''}} >Все</option>--}}
{{--                                <option value="admin" {{request('role') == 'admin' ? 'selected' :''}} >Admin</option>--}}
{{--                                <option value="moderator" {{request('role') == 'moderator' ? 'selected' :''}} >Moderator</option>--}}
{{--                                <option value="user" {{request('role') == 'user' ? 'selected' :''}} >User</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Email</label>
                            <input type="text" name="email" class="input-regular" data-validate="email" placeholder="E-mail" value="{{request('email')}}">
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список пользователей</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчества</th>
                    <th>Почта</th>
                    <th>Role</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->patronymic}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->roles[0]->name}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="/admin/user/{{$user->id}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                <a href="/admin/user/edit/{{$user->id}}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                <a href="#" title="Удалить" onclick="document.getElementById('form-delete-{{ $user->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                                <form id="form-delete-{{ $user->id }}" action="{{ route('admin.user.delete', $user->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$users->currentPage() === 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $users->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $users->lastPage(); $page++)
                    <li><a {{ $page === $users->currentPage() ? 'class="active"' : '' }} href="/admin/users?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$users->currentPage() === $users->lastPage() ? 'disabled' : '' }} href="/users?page={{ $users->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
