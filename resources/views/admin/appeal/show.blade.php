@include('admin.layouts.header')
@can('edit_content')

    <div class="container container-fluid">
        <ul class="breadcrumbs">
            <li><a href="/admin/appeals" title="Обращении">Обращении</a></li>
            <li><span>#{{$appeal->id}}</span></li>
        </ul>

        <div class="block">
            <div class="tabs">
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Основные данные</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Основные реквизиты">Основные данные</a></li>
                        </ul>

                        <div class="col-md-2 action-buttons">
                            <a href="/admin/appeal/{{$appeal->id}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" onclick="document.getElementById('form-delete-{{ $appeal->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                            <form id="form-delete-{{ $appeal->id }}" action="/admin/appeal/{{$appeal->id}}/delete" method="post">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tabs-contents">
                    <div class="active">
                        <div class="input-group">
                            <label class="input-group__title">Текст обрашение: </label>
                            <h3 name="text" type="text" placeholder="Текст обрашение" >{{ $appeal->text }}</h3>
                        </div>
                        <div class="input-group">
                            <label class="input-group__title">Статус:
                                <strong>
                                    @if($appeal->status === 1)
                                        Исполнено
                                    @elseif($appeal->status === 0)
                                        В исполнении
                                    @else
                                        Not moderated
                                    @endif
                                </strong>
                            </label>
                        </div>
                        <br>

                        <div class="input-group__title">Прикрепленные файлы</div>
                        <div class="documents">
                            @forelse($appeal->files as $file)
                                <a href="{{$file->path}}" title=""><i class="icon-file"></i> <span>{{$file->name}}</span></a>
                            @empty
                                <p>No files</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <div class="input-group__title">Написать ответ на обращение</div>
            <form action="{{ '/admin' . $appeal->path() . '/request'}}" method="post">
                @csrf
                @method('post')

                <input name="text" type="text" placeholder="Текст обрашение..." class="input-regular" required>
                <br><br>
                <div class="input-group">
                    <label class="input-group__title">Статус</label>
                    <select name="status" class="chosen no-search input-regular" required>
                        <option value="1" {{$appeal->status == '1' ? 'selected' :''}} >Исполнен</option>
                        <option value="0" {{$appeal->status == '0' ? 'selected' :''}} >На исполнении</option>
                        <option value="-1" {{$appeal->status == '-1' ? 'selected' :''}} >Not moderated</option>
                    </select>
                </div>

                <button type="submit" class="btn">Отправить</button>
            </form>
        </div>

        <div class="input-group__title">Ответы на обращение</div>
        <br>
        @forelse($appeal->requests as $request)
            <div class="block">
                <p>{{$request->created_at}}</p>
                <p>{{$request->text}}</p>
            </div>
        @empty
            <div class="block">
                <p>No comments yet!</p>
            </div>
        @endforelse
    </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
