@include('admin.layouts.header')
@can('edit_content')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Обращении</h2>

            <form action="/admin/appeals" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Поиск по ключевому слову или фразе</label>
                            <input type="text" name="text" class="input-regular" placeholder="Введите текст"  value="{{request('text')}}">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Статус</label>
                            <select name="status" class="chosen no-search input-regular" data-placeholder="Все" required>
                                <option class="hidden" value="all" hidden>Все</option>

                                <option value="all" {{request('status') === 'all' ? 'selected' :''}} >Все</option>
                                <option value="1" {{request('status') == '1' ? 'selected' :''}} >Исполнен</option>
                                <option value="0" {{request('status') == '0' ? 'selected' :''}} >На исполнении</option>
                                <option value="-1" {{request('status') == '-1' ? 'selected' :''}} >Not moderated</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Поиск  по дате (от)</label>
                            <input type="date" name="dateFrom" class="input-regular" value="{{request('dateFrom')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Поиск  по дате (до)</label>
                            <input type="date" name="dateTo" class="input-regular" value="{{request('dateTo')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список обращении</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Текст</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($appeals as $appeal)
                        <tr>
                            <td>{{$appeal->id}}</td>
                            <td>{{$appeal->text}}</td>
                            <td>
                                @if($appeal->status === 1)
                                    Исполнено
                                @elseif($appeal->status === 0)
                                    В исполнении
                                @else
                                    Not moderated
                                @endif
                            </td>
                            <td>
                                <div class="action-buttons">
                                    <a href="/admin/appeal/{{$appeal->id}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="/admin/appeal/{{$appeal->id}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" onclick="document.getElementById('form-delete-{{ $appeal->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                                    <form id="form-delete-{{ $appeal->id }}" action="/admin/appeal/{{$appeal->id}}/delete" method="post">
                                        @csrf
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="text-align:center">No appeals yet.</div>
                    @endforelse
                </tbody>
            </table>

{{--            {{ $appeals->appends(\request()->except('page'))->links() }}--}}
{{--            {{ $appeals->appends(request()->query())->links() }}--}}

            <ul class="pagination">
                <li class="previous_page"><a {{$appeals->currentPage() === 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $appeals->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $appeals->lastPage(); $page++)
                    <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="/admin/appeals?page={{$page}}">{{ $page }}</a></li>
{{--                    <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="{{$appeals->appends(\request()->except('page'))}}?page={{$page}}">{{ $page }}</a></li>--}}
{{--                    <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="{{ preg_replace('/page=([0-9]+)/', '',request()->getRequestUri()) }}?page={{$page}}">{{ $page }}</a></li>--}}
{{--                    <li><a {{ $page === $appeals->currentPage() ? 'class="active"' : '' }} href="{{request()->getRequestUri()}}?page={{$page}}">{{ $page }}</a></li>--}}
                @endfor

                <li class="next_page"><a {{$appeals->currentPage() === $appeals->lastPage() ? 'disabled' : '' }} href="/appeals?page={{ $appeals->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
