@include('admin.layouts.header')

@can('edit_content')

    <div class="container container-fluid">
        <ul class="breadcrumbs">
            <li><a href="/admin/appeals" title="Обращении">Обращении</a></li>
            <li><span>#{{$appeal->id}}</span></li>
        </ul>

        <form method="post" action="/admin/appeal/{{ $appeal->id }}" enctype="multipart/form-data" class="block" name="addORedit-doctor-form" id="addORedit-post-form">
            @csrf
            @method('put')

            <div class="tabs">
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Основные данные</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Основные реквизиты">Основные данные</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tabs-contents">
                    <div class="active">
                        <div class="input-group">
                            <label class="input-group__title"></label>
                            <input name="text" type="body" placeholder="Текст обрашение" class="input-regular" value="{{ $appeal->text }}">
                        </div>
                        <div class="input-group">
                            <label class="input-group__title">Статус</label>
                            <select name="status" class="chosen no-search input-regular" data-placeholder="" required>
                                <option value="1" {{$appeal->status == '1' ? 'selected' :''}} >Исполнен</option>
                                <option value="0" {{$appeal->status == '0' ? 'selected' :''}} >На исполнении</option>
                                <option value="-1" {{$appeal->status == '-1' ? 'selected' :''}} >Not moderated</option>
                            </select>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
            <hr>
            <div class="buttons">
                <div>
                    <button type="submit" class="btn btn--green">Сохранить</button>
                </div>
            </div>
        </form>
    </div>

@endcan

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
