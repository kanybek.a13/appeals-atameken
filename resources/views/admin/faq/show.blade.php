@include('admin.layouts.header')
@can('edit_content')

<div class="container container-fluid">
                <div class="title-block">
                    <div class="row row--multiline align-items-center">
                        <div class="col-md-4">
                            <h1 class="title-primary" style="margin-bottom: 0">FAQ</h1>
                        </div>
                        <div class="col-md-8 text-right-md text-right-lg">
                            <div class="flex-form">
                                <div>
                                    <a href="#" title="Расширенный поиск" class="btn"><i class="icon-search"></i> <span>Расширенный поиск</span></a>
                                </div>
                                <div>
                                    <form class="input-button">
                                        <input type="text" name="search" placeholder="Наименование фонда/Номер фонда" class="input-regular input-regular--solid" style="width: 282px;">
                                        <button class="btn btn--green">Найти</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="container container-fluid">
                    <ul class="breadcrumbs">
                        <li><a href="/admin/faqs" title="FAQ">FAQ</a></li>
                        <li><span>{{$faq->question}}</span></li>
                    </ul>

                    <div class="fund-header">
                        <div class="fund-header__left">
                            <div class="fund-header__id">#{{$faq->id}}</div>
                            <h1 class="fund-header__title">{{$faq->question}}</h1>
                            {{--            <div class="fund-header__organization">Туркестанский областной государственный архив</div>--}}
                        </div>
                        <div class="fund-header__right">
                            <div class="property">
                                <div class="property__title">Дата создания</div>
                                <div class="property__text">{{$faq->created_at}}</div>
                            </div>
                            <div class="property">
                                <div class="property__title">Дата изменения	</div>
                                <div class="property__text">{{$faq->updated_at}}<br>
                                    Администратор Panama DC</div>
                            </div>
                        </div>
                    </div>

                    <div class="block">
                        <div class="tabs">

                            <div class="mobile-dropdown">
                                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                                <div class="mobile-dropdown__desc">
                                    <ul class="tabs-titles">
                                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tabs-contents">
                                <div class="active">
                                    <div class="input-group">
                                        <label class="input-group__title">Вопрос</label>
                                        <input type="text" name="question" value="{{$faq->question}}" placeholder="Название" class="input-regular" disabled>
                                    </div>

                                    <div class="input-group">
                                        <label class="input-group__title">Ответ</label>
                                        <textarea name="answer" placeholder="Ответ" class="input-regular">{{$faq->answer}}</textarea>
{{--                                        <textarea name="answer" placeholder="Ответ" class="input-regular">{{$equipment->description}}--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
