@include('admin.layouts.header')
@can('edit_content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">FAQ</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        <form class="input-button" action="/admin/faqs" method="GET">
                            <input type="text" name="text" value="{{request('text')}}" placeholder="Наименование faq/Ответ faq" class="input-regular input-regular--solid" style="width: 282px;">
                            <button class="btn btn--green">Найти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <main class="main">
        <div class="container container-fluid">

            <div class="block">
                <h2 class="title-secondary">Список FAQ</h2>

                <table class="table records">
                    <colgroup>
                        <col span="1" style="width: 3%;">
                        <col span="1" style="width: 20%;">
                        <col span="1" style="width: 12%;">
                        <col span="1" style="width: 20%;">
                        <col span="1" style="width: 15%;">
                        <col span="1" style="width: 15%;">
                        <col span="1" style="width: 15%;">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Вопрос</th>
    {{--                                    <th>Ответ</th>--}}
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($faqs as $faq)
                        <tr>
                            <td>{{$faq->id}}</td>
                            <td>{{$faq->question}}</td>
    {{--                                        <td>{{$faq->answer}}</td>--}}
                            <td>
                                <div class="action-buttons">
                                    <a href="/admin/faq/{{$faq->id}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="/admin/faq/{{$faq->id}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
    {{--                                                <a href="/admin/faq/{{$faq->id}}/delete" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>--}}

                                    <form action="/admin/faq/{{$faq->id}}/delete" method="post">
    {{--                                                    <input class="icon-btn icon-btn--pink icon-delete" type="submit" value="" />--}}

                                        <input class="btn btn-default" type="submit" value="Delete" />
                                        <input type="hidden" name="_method" value="post" />
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>

                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

@endcan
@extends('admin.layouts.footer')

@section('content')
    <!--Only this page's scripts-->

    <!---->
@endsection
