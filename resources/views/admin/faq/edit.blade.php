@include('admin.layouts.header')
@can('edit_content')

    <div class="container container-fluid">
                <ul class="breadcrumbs">
                    <li><a href="/admin/faqs" title="Оборудование">FAQ</a></li>
                    <li><span>{{$faq->question}}</span></li>
                </ul>

                <form class="block" method="post" action="/admin/faq/{{$faq->id}}">
                    @csrf
                    @method('put')

                    <input type="hidden" name="id" value="{{$faq->id}}">

                    <div class="tabs-contents">
                        <div class="active">

                            <div class="input-group">
                                <label class="input-group__title"> Наименование оборудование</label>
                                <input type="text" name="question" value="{{$faq->question}}" placeholder="Название" class="input-regular">
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Описание</label>
                                <textarea name="answer" placeholder="Описание" class="input-regular">{{$faq->answer}}
                               </textarea>
                            </div>
                            <br>
                        </div>
                    </div>
                    <hr>
                    <div class="buttons">
                        <div>
                            <button type="submit" class="btn btn--green">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->

    <!---->
@endsection
