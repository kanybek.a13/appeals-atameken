@include('admin.layouts.header')
@can('edit_content')


        <main class="main">

            <div class="container container-fluid">
                <ul class="breadcrumbs">
                    <li><a href="/admin/faqs" title="FAQ">FAQ</a></li>
                    <li><span>Новый</span></li>
                </ul>

                <form class="block" method="post" action="/admin/faqs">
                    @csrf
                    @method('post')

                    <div class="tabs-contents">
                        <div class="active">

                            <div class="input-group">
                                <label class="input-group__title"> Наименование оборудование</label>
                                <input type="text" name="question" value="{{ old('question')}}" placeholder="Название" class="input-regular">
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Описание</label>
                                <textarea name="answer" placeholder="Описание" class="input-regular">{{old('answer')}}</textarea>
                            </div>
                            <br>
                        </div>
                    </div>
                    <hr>
                    <div class="buttons">
                        <div>
                            <button type="submit" class="btn btn--green">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
