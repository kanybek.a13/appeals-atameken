    <aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/admin/index" title="Главная" class="logo"><img src="/admin/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown">
                <a href="javascript:;" title="Заявка"><i class="icon-organizations"></i>Index</a>
                <ul>
                    <li><a href="/admin/index" title="Список заявок">Admin index</a></li>
                </ul>
                <ul>
                    <li><a href="/" title="Список заявок">App index</a></li>
                </ul>
            </li>

            @can('edit_content')
                @can('admin')
                    <li class="dropdown">
                        <a href="javascript:;" title="Пользователи"><i class="icon-archive"></i> Пользователи</a>
                        <ul>
                            <li><a href="/admin/users" title="Список пользователей">Список пользователей</a></li>
                            <li><a href="/admin/new/user" title="Добавить" class="add">+Добавить</a></li>
                        </ul>
                    </li>
                @endcan

                <li class="dropdown">
                    <a href="javascript:;" title="Заявка"><i class="icon-organizations"></i> Заявка</a>
                    <ul>
                        <li><a href="/admin/appeals" title="Список заявок">Список заявок</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="javascript:;" title="FAQ"><i class="icon-reports"></i> FAQ</a>
                    <ul>
                        <li><a href="/admin/faqs" title="Список оборудований">Список FAQ</a></li>
                        <li><a href="/admin/faq/new" title="Добавить" class="add">+Добавить</a></li>
                    </ul>
                </li>
            @endcan
        </ul>
    </div>
</aside>
