<section>
    <div class="container">
        <h1 class="title-primary">Обращение</h1>
        <div class="card">
            <div class="card__info">
                <div class="card__date">{{$appeal->updated_at}}</div>
                <div class="card__status green">{{$appeal->status === 'completed' ? "Исполнено" : "В исполнении"}}</div>
            </div>

            <hr align="left" width="300" size="4" color="#ff9900" />
            <div class="input-group__title">Текст обращения</div>
            <div class="plain-text">
            </div>

            <form action="/admin/appeal/{{ $appeal->id }}" method="post">
                @method('put')

                <hr align="left" width="300" size="4" color="#ff9900" />
                <div class="input-group__title">Прикрепленные файлы</div>
                <div class="documents">

                    @foreach($appeal->files as $file)
                    <a href="{{$file->path}}" title=""><i class="icon-file"></i> <span>{{$file->name}}</span></a>

                    <br>
                    <div class="action-buttons">
                        <form action="/admin/appeal/{{ $appeal->id }}/file/{{ $file->id }}/delete" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="post" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    @endforeach
                </div>

                <div class="card">
                    <hr align="left" width="300" size="4" color="#ff9900" />
                    <div class="input-group__title">Ответы на обращение</div>

                    <div class="card">
                        @if(auth()->user()->id===$appeal->owner_id)
                        <form action="{{ $appeal->path() . '/request'}}" method="post">
                            @csrf
                            @method('post')
                            <div class="input-group__title">Написать коментарий</div>
                            <br>
                            <input class="w-full" name="text" placeholder="Техт коментарий...">
                        </form>
                        @endif
                    </div>

                    @forelse($appeal->requests as $appeal_request)
                    <div class="plain-text">
                        <hr align="left" width="300" size="4" color="#ff9900" />

                        <p>{{$appeal_request->created_at}}</p>
                        <p>{{$appeal_request->text}}</p>
                    </div>

                    <div class="action-buttons">
                        <form action="/admin/appeal/{{ $appeal_request->id }}/request/{{ $appeal_request->id }}/delete" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="post" />
                            <input type="hidden" name="_method" value="post" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    @empty
                    <div class="card">
                        <br><p>No comments yet!</p>
                    </div>
                    @endforelse
                </div>
        </div>
</section>
