<?php
return [
    'title' => 'Атамекен русский',
    'main'=> 'Главная',
    'appeal'=> 'Обращении',
    'faq'=> 'Вопросы',
    'submit_appeal' => 'Подать обращение',
    'login' => 'Войти',
    'register' => 'Регистрация',
    'my_personal_data'=> 'Личные данные',
    'my_appeals'=> 'Мои обращении',
    'logout'=> 'Выйти',
];
