<?php
return [
    'title' => 'Atameken english',
    'main'=> 'Main',
    'appeal'=> 'Appeals',
    'faq'=> 'FAQ',
    'submit_appeal' => 'Submit an appeal',
    'login' => 'Login',
    'register' => 'Register',
    'my_personal_data'=> 'Personal data',
    'my_appeals'=> 'My appeals',
    'logout'=> 'Logout',
];
