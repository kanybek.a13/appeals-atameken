<?php
return [
    'title' => 'Атамекен қазақша',
    'main'=> 'Басты бет',
    'appeal'=> 'Өтініштер',
    'faq'=> 'Жиі қойылатын сұрақтар',
    'submit_appeal' => 'Өтініш беру',
    'login' => 'Кiру',
    'register' => 'Тіркелу',
    'my_personal_data'=> 'Жеке мәліметтер',
    'my_appeals'=> 'Менің Өтініштерім',
    'logout'=> 'Шығу',
];
