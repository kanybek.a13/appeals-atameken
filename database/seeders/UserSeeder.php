<?php

namespace Database\Seeders;

use App\Models\Ability;
use App\Models\Faq;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_ability = Ability::firstOrCreate([
            'name' => 'admin',
            'label' => 'admin ',
        ]);

        $edit_ability = Ability::firstOrCreate([
            'name' => 'edit_content',
            'label' => 'edit_content',
        ]);

        $view_ability = Ability::firstOrCreate([
            'name' => 'view_content',
            'label' => 'view_content',
        ]);

//    --------------------------------------------------------------------------

        $admin_role = Role::firstOrCreate([
            'name' => 'admin',
            'label' => 'admin role',
        ]);

        $moderator_role = Role::firstOrCreate([
            'name' => 'moderator',
            'label' => 'moderator role',
        ]);

        $user_role = Role::firstOrCreate([
            'name' => 'user',
            'label' => 'user role',
        ]);

//    --------------------------------------------------------------------------

        $admin_role->allowTo($admin_ability);
        $admin_role->allowTo($edit_ability);
        $admin_role->allowTo($view_ability);


        $moderator_role->allowTo($edit_ability);
        $moderator_role->allowTo($view_ability);

        $user_role->allowTo($view_ability);

//    --------------------------------------------------------------------------

        $aidar = User::create([
            'name' => 'Aidar',
            'surname' => 'Kanybek',
            'patronymic' => 'Kanybek',
            'iin' => '999999999999',
            'phone' => '+9 999 999 99 99',
            'email' => 'kanybek.a13@gmail.com',
            'password' => Hash::make('aidar'),
        ]);

        $admin = User::create([
            'name' => 'admin_name',
            'surname' => 'admin_surname',
            'patronymic' => 'admin_patronymic',
            'iin' => '111111111111',
            'phone' => '+7 777 777 77 77',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin'),
        ]);

        $moderator = User::create([
            'name' => 'moderator_name',
            'surname' => 'moderator_surname',
            'patronymic' => 'moderator_patronymic',
            'iin' => '222222222222',
            'phone' => '+7 777 777 77 77',
            'email' => 'moderator@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('moderator'),
        ]);

        $user = User::create([
            'name' => 'user_name',
            'surname' => 'user_surname',
            'patronymic' => 'user_patronymic',
            'iin' => '333333333333',
            'phone' => '+7 777 777 77 77',
            'email' => 'user@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('user'),
        ]);

//    --------------------------------------------------------------------------

        $aidar->assignRole($admin_role);
        $admin->assignRole($admin_role);
        $moderator->assignRole($moderator_role);
        $user->assignRole($user_role);

//    --------------------------------------------------------------------------

        Faq::factory()->count(4)->create();

    }
}
